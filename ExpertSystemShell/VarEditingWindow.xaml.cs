﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell
{
    /// <summary>
    /// Логика взаимодействия для VarEditingWindow.xaml
    /// </summary>
    public partial class VarEditingWindow : Window
    {
        public Variable var { get; set; }
        public Variable varCopy { get; }


        public VarEditingWindow()
        {
            InitializeComponent();
            varCopy = new Variable();
            var = new Variable();
            tbVarName.Text = "Variable" + (MainWindow.CurrentShell.CurKnowledgeBase.variables.Count() + 1).ToString();
            tbVarName.Focus();
        }

        public VarEditingWindow(Variable v)
        {
            InitializeComponent();
            var = v;
            varCopy = new Variable();
            varCopy.Name = v.Name;
            varCopy.Type = v.Type;
            varCopy.Dom = v.Dom;
            varCopy.Text = v.Text;
            Initialize(v);
    }

        private void Initialize(Variable v)
        {
            tbVarName.Text = varCopy.Name;
            tbQuestionText.Text = varCopy.Text;
            if (varCopy.Type == VariableEnum.In)
                rbZapr.IsChecked = true;
            else
                rbVyvod.IsChecked = true;
            if (CheckForUsing(v))
            {
                cbDomens.IsEnabled = false;
                tbAddDomen.IsEnabled = false;
            }
            lvVarValues.SelectedIndex = 0;
        }

        private void ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            cbLoad();
           // if ()
        }

        private void cbLoad()
        {
            cbDomens.Items.Clear();
            foreach (Domain d in MainWindow.CurrentShell.CurKnowledgeBase.domains)
                cbDomens.Items.Add(d.Name);
            if (varCopy.Name != null)
            {
                int domIndex = MainWindow.CurrentShell.CurKnowledgeBase.domains
                    .IndexOf(MainWindow.CurrentShell.CurKnowledgeBase.domains
                    .Where(element => varCopy.Dom.Name == element.Name).FirstOrDefault());
                cbDomens.SelectedIndex = domIndex;
            }
            else
                cbDomens.SelectedIndex = 0;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
             lvVarValues.Items.Clear();
            // var comboBox = sender as ComboBox;

            string value = cbDomens.SelectedItem as string;
            if (value != null)
            {
                Domain result = MainWindow.CurrentShell.CurKnowledgeBase.domains.Where(element => value == element.Name).First();
                int nCount = 1;
                foreach (string str in result.Values)
                {
                    lvVarValues.Items.Add(new MyItem() { Number = nCount, Content = str });
                    nCount++;
                }
            }
        }
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (rbZapr != null && tbQuestionText != null && rbZapr.IsChecked == true)
                tbQuestionText.IsEnabled = true;
            else
            {
                if ((rbVyvod != null && tbQuestionText != null)|| (rbZaprVyvod != null && tbQuestionText != null))
                    tbQuestionText.IsEnabled = false;
            }
        }

        private void tbAddDomen_Click(object sender, RoutedEventArgs e)
        {
            DomainEditingWindow domainEditingWindow;           
            domainEditingWindow = new DomainEditingWindow();          
            domainEditingWindow.Owner = this;

            if (domainEditingWindow.ShowDialog() == true)
            {
                MainWindow.CurrentShell.CurKnowledgeBase.AddOrEditDomain(domainEditingWindow.domainCopy, WindowMode.add, -1);
                cbLoad();
            }
            else
            {

            }
        }

        private void tbEditDomen_Click(object sender, RoutedEventArgs e)
        {
            Domain d = null;
            var item = cbDomens.SelectedItem;
            if (item != null)
                d = (Domain)MainWindow.CurrentShell.CurKnowledgeBase.domains.Where(element => item.ToString() == element.Name).First();

            DomainEditingWindow domainEditingWindow;
            domainEditingWindow = new DomainEditingWindow(d);
            domainEditingWindow.Owner = this;

            if (domainEditingWindow.ShowDialog() == true)
            {
                MainWindow.CurrentShell.CurKnowledgeBase.AddOrEditDomain(domainEditingWindow.domainCopy, WindowMode.edit, cbDomens.SelectedIndex);
                cbLoad();
            }
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbVarName.Text))
            {
                MessageBox.Show("Поля не должны быть пустыми");
                return;
            }
            else
            {
                if ((rbZapr.IsChecked == true) && String.IsNullOrWhiteSpace(tbQuestionText.Text))
                    varCopy.Text = tbVarName.Text + "?";
                else
                    varCopy.Text = tbQuestionText.Text ?? "";

                varCopy.Name = tbVarName.Text;

                var item = cbDomens.SelectedItem;
                Domain d = (Domain) MainWindow.CurrentShell.CurKnowledgeBase.domains.Where(element => item.ToString() == element.Name).First();
                varCopy.Dom = d;

                if (rbVyvod.IsChecked == true)
                    varCopy.Type = VariableEnum.Out;
                if (rbZapr.IsChecked == true)
                    varCopy.Type = VariableEnum.In;
                if (rbZaprVyvod.IsChecked == true)
                    varCopy.Type = VariableEnum.InOut;

                this.DialogResult = true;
                Close();
            }


        }
  
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        public static bool CheckForUsing(Variable var)
        {
            bool isUsed = false;
            for (int i = 0; i < MainWindow.CurrentShell.CurKnowledgeBase.rules.Count; i++)
            {
                Fact use = MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConditionList.Where(element => var == element.Var).FirstOrDefault();
                Fact use1 = MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConclusionList.Where(element => var == element.Var).FirstOrDefault();
                if (use != null || use1 != null)
                    isUsed = true;
            }

            return isUsed;
        }
    }
}
