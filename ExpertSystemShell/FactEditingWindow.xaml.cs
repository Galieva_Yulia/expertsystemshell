﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell
{
    /// <summary>
    /// Логика взаимодействия для FactEditingWindow.xaml
    /// </summary>
    public partial class FactEditingWindow : Window
    {
        public Fact CurFact;
        private int type;
        public FactEditingWindow()
        {
            InitializeComponent();
            CurFact = new Fact();
            CbInitialize();
        }

        public FactEditingWindow(int type)
        {
            InitializeComponent();
            CurFact = new Fact();
            this.type = type;
            CbInitialize();
        }

        public FactEditingWindow(Fact f)
        {
            InitializeComponent();
            CurFact = f;
            CbInitialize();
            cbVar.SelectedIndex = MainWindow.CurrentShell.CurKnowledgeBase.variables
                .IndexOf(MainWindow.CurrentShell.CurKnowledgeBase.variables
                .Where(element => element == f.Var).FirstOrDefault());

            for (int i = 0; i < MainWindow.CurrentShell.CurKnowledgeBase.variables.Count; i++)
            {
                if (MainWindow.CurrentShell.CurKnowledgeBase.variables[i] == f.Var)
                    cbVar.SelectedIndex = i;
            }

            for (int i =0; i< f.Var.Dom.Values.Count; i++)
            {
                if (f.Var.Dom.Values[i] == f.Value)
                cbVarDomen.SelectedIndex = i;
            }
          

        }

        private void CbInitialize()
        {
            cbVar.Items.Clear();
            cbVarDomen.Items.Clear();

            foreach (Variable v in MainWindow.CurrentShell.CurKnowledgeBase.variables)
            {
                if (type == 1 && (v.Type == VariableEnum.Out || v.Type == VariableEnum.InOut))
                    cbVar.Items.Add(v.Name);
                else if (type == 0)
                    cbVar.Items.Add(v.Name);
            }
            cbVar.SelectedIndex = 0;
            cbVarDomen.SelectedIndex = 0;
        }

        private void CbVar_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbVarDomen.Items.Clear();
            string value = cbVar.SelectedItem as string;
            if (value != null)
            {
                Variable result = (Variable) MainWindow.CurrentShell.CurKnowledgeBase.variables.Where(element => value == element.Name).First();
               foreach (var v in result.Dom.Values)
                    cbVarDomen.Items.Add(v);
                cbVarDomen.SelectedIndex = 0;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CurFact.Var = (Variable)MainWindow.CurrentShell.CurKnowledgeBase.variables.Where(element => cbVar.SelectedItem.ToString() == element.Name).First();
            CurFact.Value = cbVarDomen.SelectedItem.ToString();
            this.DialogResult = true;
            Close();
        }

        private void btnAddVar_Click(object sender, RoutedEventArgs e)
        {
            openVarEditingWindow();
        }

        private void openVarEditingWindow()
        {
            VarEditingWindow varEditingWindow;

                varEditingWindow = new VarEditingWindow();
           
            varEditingWindow.Owner = this;

            if (varEditingWindow.ShowDialog() == true)
            {
                MainWindow.CurrentShell.CurKnowledgeBase.AddOrEditVariable(varEditingWindow.varCopy, WindowMode.add, -1);
                CbInitialize();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
