﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ExpertSystemShell
{
    /// <summary>
    /// Логика взаимодействия для RulesEditingWindow.xaml
    /// </summary>
    public partial class RulesEditingWindow : Window
    {
        enum FactType
        {
            condition = 0,
            conclusion = 1
        }

        public Rule rule { get; set; }
        public Rule ruleCopy { get; }

        public RulesEditingWindow()
        {
            InitializeComponent();
            rule = new Rule();
            ruleCopy = new Rule();
            tbRuleName.Text = "Rule" + (MainWindow.CurrentShell.CurKnowledgeBase.rules.Count + 1).ToString();
        }

        public RulesEditingWindow(Rule r)
        {
            InitializeComponent();
            rule = r;

            ruleCopy = new Rule();
            for (int i = 0; i < r.ConditionList.Count; i++)
            {
                var cond = r.ConditionList[i];
                ruleCopy.ConditionList.Add(cond);
            }

            for (int i = 0; i < r.ConclusionList.Count; i++)
            {
                var cond = r.ConclusionList[i];
                ruleCopy.ConclusionList.Add(cond);
            }

            ruleCopy.Name = r.Name.ToString();
            ruleCopy.Explanation = r.Explanation;
            ruleCopy.Content = r.Content;
            Initialize();
        }

        private void Initialize()
        {
            tbRuleName.Text = ruleCopy.Name;
            tbExplanation.Text = ruleCopy.Explanation;
            foreach (var rule in ruleCopy.ConditionList)
                lvRuleCondition.Items.Add(new MyItem() { Content = rule.Var.Name + "==" + rule.Value.ToString() });

            foreach (var rule in ruleCopy.ConclusionList)
                lvRuleConclusion.Items.Add(new MyItem() { Content = rule.Var.Name + ":=" + rule.Value.ToString() });
            lvRuleConclusion.SelectedIndex = 0;
            lvRuleCondition.SelectedIndex = 0;
        }

        private void tbAddConditionRule_Click(object sender, RoutedEventArgs e)
        {
            openFactEditingWindow(WindowMode.add, FactType.condition);
        }

        private void tbEditConditionRule_Click(object sender, RoutedEventArgs e)
        {
            openFactEditingWindow(WindowMode.edit, FactType.condition);
        }


        private void openFactEditingWindow(WindowMode mode, FactType factType)
        {
            FactEditingWindow factEditingWindow;
           
            switch (mode)
            {
                case (WindowMode.add):
                    // factEditingWindow = new FactEditingWindow();
                    int type = -1;
                    if (factType == FactType.condition)
                        type = 0;
                    else type = 1;
                    factEditingWindow = new FactEditingWindow(type);
                    factEditingWindow.Owner = this;

                    if (factEditingWindow.ShowDialog() == true)
                    {
                        if (factType == FactType.condition)
                        {
                            MainWindow.CurrentShell.CurKnowledgeBase.AddOrEditRuleCondition(ruleCopy, factEditingWindow.CurFact, WindowMode.add, -1);
                            ListViewOp.UpdateLv(ruleCopy.ConditionList, lvRuleCondition, "==");
                        }
                        else
                        {
                            MainWindow.CurrentShell.CurKnowledgeBase.AddOrEditRuleConclusion(ruleCopy, factEditingWindow.CurFact, WindowMode.add, -1);
                            ListViewOp.UpdateLv(ruleCopy.ConclusionList, lvRuleConclusion, ":=");
                        }
                    }
                    break;

                case (WindowMode.edit):
                    {
                        if (factType == FactType.condition)
                        {
                            
                            int index = lvRuleCondition.SelectedIndex;
                            if (index == -1)
                            {
                                MessageBox.Show("Веберете редактируемый факт");
                                return;
                            }
                            Fact f = ruleCopy.ConditionList[index];
                            if (f != null)
                            {
                                factEditingWindow = new FactEditingWindow(f);
                                factEditingWindow.Owner = this;

                                if (factEditingWindow.ShowDialog() == true)
                                {
                                    //по закрытию окна редактирования факта добавляем факт в список посылок
                                    MainWindow.CurrentShell.CurKnowledgeBase.AddOrEditRuleCondition(ruleCopy, factEditingWindow.CurFact, mode, lvRuleCondition.SelectedIndex);
                                    ListViewOp.UpdateLv(ruleCopy.ConditionList, lvRuleCondition, "==");
                                }
                            }
                        }
                        else
                        {
                            int index = lvRuleConclusion.SelectedIndex;
                            if (index == -1)
                            {
                                MessageBox.Show("Веберете редактируемый факт");
                                return;
                            }
                            Fact f = ruleCopy.ConclusionList[index];
                            if (f != null)
                            {
                                factEditingWindow = new FactEditingWindow(f);
                                factEditingWindow.Owner = this;

                                if (factEditingWindow.ShowDialog() == true)
                                {
                                    //по закрытию окна редактирования факта добавляем факт в список посылок
                                    MainWindow.CurrentShell.CurKnowledgeBase.AddOrEditRuleConclusion(ruleCopy, factEditingWindow.CurFact, mode, lvRuleCondition.SelectedIndex);
                                    ListViewOp.UpdateLv(ruleCopy.ConclusionList, lvRuleConclusion, ":=");

                                }
                            }
                        }
                    }
                    break;
            }

          
        }

      


        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbRuleName.Text)|| lvRuleConclusion.Items.Count == 0 || lvRuleCondition.Items.Count == 0)
            {
                MessageBox.Show("Поля не должны быть пустыми");
                return;
            }
            else
            {
                ruleCopy.SetText();
                ruleCopy.Name = tbRuleName.Text;
                ruleCopy.Explanation = tbExplanation.Text;
                this.DialogResult = true;
                Close();
            }
        }

        private void btnAddConclusionRule_Click(object sender, RoutedEventArgs e)
        {
            openFactEditingWindow(WindowMode.add, FactType.conclusion);
        }

        private void btnEditConclusionRule_Click(object sender, RoutedEventArgs e)
        {
            openFactEditingWindow(WindowMode.edit, FactType.conclusion);
        }

        private void btnDeleteConditionRule_Click(object sender, RoutedEventArgs e)
        {
            lvRemoveAt(ruleCopy.ConditionList, lvRuleCondition);
            ListViewOp.UpdateLv(ruleCopy.ConditionList, lvRuleCondition, "==");
        }

        private void btnDeleteConclusionRule_Click(object sender, RoutedEventArgs e)
        {
            lvRemoveAt(ruleCopy.ConclusionList, lvRuleConclusion);
            ListViewOp.UpdateLv(ruleCopy.ConclusionList, lvRuleConclusion, ":=");
        }

        private void lvRemoveAt(List<Fact> list, ListView lv)
        {
            if (lv.SelectedIndex != -1)
                list.RemoveAt(lv.SelectedIndex);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
