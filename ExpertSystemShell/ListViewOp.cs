﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ExpertSystemShell
{
    public static class ListViewOp
    {
        public static void FillListView_NumberAndContent(List<string> list, ListView lv)
        {
            lv.Items.Clear();
            int nCount = 1;
            foreach (var el in list)
            {
                lv.Items.Add(new MyItem() { Number = nCount, Content = el });
                nCount++;
            }
        }

        public static void UpdateLv(List<Fact> list, ListView lv, string sym)
        {
            lv.Items.Clear();
            foreach (var c in list)
                lv.Items.Add(new MyItem() { Content = c.Var.Name + sym + c.Value.ToString() });
        }


    }
}
