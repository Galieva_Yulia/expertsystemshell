﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
    [Serializable]
    class SerializableKnowledgeBase
    {
        public List<Domain> domainsList { get; set; }
        public List<Variable> variablesList { get; set; }
        public List<Rule> rulesList { get; set; }

        public SerializableKnowledgeBase(KnowledgeBase kb)
        {
            domainsList = kb.domains.ToList();
            variablesList = kb.variables.ToList();
            rulesList = kb.rules.ToList();
        }

        public KnowledgeBase deserialize()
        {
            KnowledgeBase kb = new KnowledgeBase();
            foreach (var dom in domainsList)
                kb.domains.Add(dom);

            foreach (var var in variablesList)
                kb.variables.Add(var);

            foreach (var rule in rulesList)
                kb.rules.Add(rule);
            return kb;
        }
    }
}
