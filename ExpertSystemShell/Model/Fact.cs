﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
    [Serializable]
    public class Fact
    {
        public Variable Var { get; set; }
        public string Value { get; set; }

        public Fact() { }

        public Fact(Variable Var, string Value)
        {
            this.Var = Var;
            this.Value = Value;
        }
    }
}
