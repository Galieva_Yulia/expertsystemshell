﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell
{
    [Serializable]
    public class Domain
    {
        public string Name { get; set; }
        public List<string> Values { get; set; }

        public Domain()
        {
            Values = new List<string>();
        }

        public Domain(string Name, List<string> Values)
        {
            this.Name = Name;
            this.Values = Values;
        }

        public Domain Clone()
        {
            return new Domain()
            {
                Name = Name,
                Values = Values.ToList()
            };
        }
    }
}
