﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell
{
    public class KnowledgeBase
    {
        public ObservableCollection<Domain> domains { get ; set; }
        public ObservableCollection<Variable> variables { get; set; }
        public ObservableCollection<Rule> rules { get; set; }

        public KnowledgeBase()
        {
            domains = new ObservableCollection<Domain>();
            variables = new ObservableCollection<Variable>();
            rules = new ObservableCollection<Rule>();
        }

        public void AddOrEditDomain(Domain dom, WindowMode mode, int index)
        {
            if (mode == WindowMode.add)
            {
                if (index != -1)
                    domains.Insert(index + 1, dom);
                else
                    domains.Add(dom);
            }
            else
            {
                if (index != -1)
                {
                    domains[index] = dom;
                }

                //перезапишем домены в переменные
                foreach (Variable v in variables)
                {
                    if (v.Dom.Name == dom.Name)
                        v.Dom = dom;
                }
            }
        }

        public void DeleteDomain(int index)
        {
            domains.RemoveAt(index);
        }

        public void DeleteVars(int index)
        {
            variables.RemoveAt(index);
        }

        internal void AddOrEditVariable(Variable var, WindowMode mode, int index)
        {
            if (mode == WindowMode.add)
            {
                if (index != -1)
                    variables.Insert(index + 1, var);
                else
                    variables.Add(var);
            }
            else
            {
                Variable before = null;
                if (index != -1)
                {
                    before = variables[index];
                    variables[index] = var;
                }


                for (int i = 0; i < rules.Count; i++)
                {



                    int use = MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConditionList
                   .IndexOf(MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConditionList
                   .Where(element => before == element.Var).FirstOrDefault());

                    int use1 = MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConclusionList
                                    .IndexOf(MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConclusionList
                                    .Where(element => before == element.Var).FirstOrDefault());

                    // Fact use = MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConditionList.Where(element => before == element.Var).FirstOrDefault();
                    // Fact use1 = MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConclusionList.Where(element => before == element.Var).FirstOrDefault();
                    if (use != -1)
                    {
                        MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConditionList[use].Var = var;
                        //в самом тексте
                        MainWindow.CurrentShell.CurKnowledgeBase.rules[i].SetText();
                    }

                    if (use1 != -1)
                    {
                        MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConclusionList[use1].Var = var;
                        MainWindow.CurrentShell.CurKnowledgeBase.rules[i].SetText();
                    }
                }

            }
        }

        internal void AddOrEditRuleConclusion(Rule rule, Fact curFact, WindowMode mode, int index)
        {
            if (mode == WindowMode.add)
            {
                if (index != -1)
                    rule.ConclusionList.Insert(index + 1, curFact);
                else
                    rule.ConclusionList.Add(curFact);
            }
            else
                if (index != -1)
                rule.ConclusionList[index] = curFact;
        }

        internal void AddOrEditRuleCondition(Rule rule, Fact curFact, WindowMode mode, int index)
        {
            if (mode == WindowMode.add)
            {
                rule.ConditionList.Add(curFact);
            }
            else
                if (index != -1)
                rule.ConditionList[index] = curFact;
        }

        internal void AddOrEditRule(Rule ruleCopy, WindowMode mode, int index)
        {
            if (mode == WindowMode.add)
            {
                if (index != -1)
                    rules.Insert(index, ruleCopy);
                else
                    rules.Add(ruleCopy);
            }
            else
            {
                if (index != -1)
                {
                    rules[index] = ruleCopy;
                }
            }
        }

        internal void DeleteRules(int index)
        {
            rules.RemoveAt(index);
        }


        /* public static bool CheckForUsing(Variable var)
        {
            bool isUsed = false;
            for (int i = 0; i < MainWindow.CurrentShell.CurKnowledgeBase.rules.Count; i++)
            {
                Fact use = MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConditionList.Where(element => var == element.Var).FirstOrDefault();
                Fact use1 = MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConclusionList.Where(element => var == element.Var).FirstOrDefault();
                if (use != null || use1 != null)
                    isUsed = true;
            }

            return isUsed;
        }*/
    }
}
