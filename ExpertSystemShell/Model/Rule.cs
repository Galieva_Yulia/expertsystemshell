﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
    [Serializable]
   public class Rule
    {
        public string Name { get; set; }
        public List<Fact> ConditionList { get; set; }
        public List<Fact> ConclusionList { get; set; }
        public string Explanation { get; set; }
      //  private string m_Text;

        public string Content { get; set; }
       

        public void SetText()
        {
            Content = "IF(" + ConditionList[0].Var.Name + " = " + ConditionList[0].Value;
            for (int i = 1, Count = ConditionList.Count; i < Count; i++)
            {
                Content += " AND " + ConditionList[i].Var.Name + " = " + ConditionList[i].Value;
            }

            Content += ") THEN " + ConclusionList[0].Var.Name + " = " + ConclusionList[0].Value;

            for (int i = 1, Count = ConclusionList.Count; i < Count; i++)
            {
                Content += ", " + ConclusionList[i].Var.Name + " = " + ConclusionList[i].Value;
            }
        }

        public Rule()
        {
            ConditionList = new List<Fact>();
            ConclusionList = new List<Fact>();
        }

        public Rule(string Name, List<Fact> ConditionList, List<Fact> ResultList, string Reason)
        {
            this.Name = Name;
            this.ConditionList = ConditionList;
            this.ConclusionList = ResultList;
            this.Explanation = Reason;

            SetText();
        }
    }
}
