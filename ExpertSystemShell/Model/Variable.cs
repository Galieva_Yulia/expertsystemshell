﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
    public enum VariableEnum
    {
        In,
        Out,
        InOut
    }
    [Serializable] 
    public class Variable
    {
        public string Name { get; set; }
        public VariableEnum Type { get; set; }
        public Domain Dom { get; set; }
        public string Text { get; set; }

        public string Content { get { return Dom.Name; } }
        public string TypeContent
        {
            get
            {
                string type = "";
                if (Type == VariableEnum.In)
                    type = "Запрашиваемая";
                if (Type == VariableEnum.Out)
                    type = "Выводимая";
                if (Type == VariableEnum.InOut)
                    type = "Запрашиваемо-выводимая";
                return type;
            }
        }

        public Variable() { }

        public Variable(string Name, VariableEnum Type, Domain Dom, string Text = "")
        {
            this.Name = Name;
            this.Type = Type;
            this.Dom = Dom;
            this.Text = Text;
        }

        public string VarTypeToString()
        {
            switch (Type)
            {
                case VariableEnum.In: return "Запрашиваемая";
                case VariableEnum.Out: return "Выводимая";
                case VariableEnum.InOut: return "Выводимо-запрашиваемая";
            }

            return "";
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
