﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Ke
{
    public class Shell
    {
        public KnowledgeBase CurKnowledgeBase { get; set; }
        public Consultation curConsultation { get; set; }
       

        public Shell()
        {
            CurKnowledgeBase = new KnowledgeBase();
        }

        public void LoadSystem(string FileName)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            using (Stream stream = File.Open(FileName, FileMode.Open))
            {
                SerializableKnowledgeBase kb = (SerializableKnowledgeBase)formatter.Deserialize(stream);
                CurKnowledgeBase = kb.deserialize();
               
            }
        }

        public void CreateSystem()
        {
            CurKnowledgeBase = new KnowledgeBase();
        }

        public void SaveSystem(string FileName)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            using (Stream stream = File.Open(FileName, FileMode.OpenOrCreate))
            {

                formatter.Serialize(stream, new SerializableKnowledgeBase(CurKnowledgeBase));
            }
        }
    }
}
