﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
    public class Consultation
    {
        public List<Rule> workedRules;
        public List<Fact> memory;

        public Fact goal { get; set; }

        private KnowledgeBase knowledgeBase;
        bool isError = false; //ошибка при консультации (неверный порядок правил и т.д.)

        public Consultation(Variable goal, KnowledgeBase knowledgeBase)
        {
            workedRules = new List<Rule>();
            memory = new List<Fact>();
            this.goal = new Fact();
            this.goal.Var = goal;
            this.knowledgeBase = knowledgeBase;
        }
    }
}
