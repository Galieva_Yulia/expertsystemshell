﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell
{
    /// <summary>
    /// Логика взаимодействия для ExplanationTreeWindow.xaml
    /// </summary>
    public partial class ExplanationTreeWindow : Window
    {
        private bool isExpanded = true;
        public ExplanationTreeWindow()
        {
            InitializeComponent();
        }

        public ExplanationTreeWindow(Fact goal, List<Rule> workedRules, List<Fact> memory)
        {
            InitializeComponent();
            ShowExplanation(goal,workedRules, memory);
        }

        private void ShowExplanation(Fact goal, List<Rule> workedRules, List<Fact> memory)
        {
            explanationTree.Items.Clear();
            TreeViewItem newChild = new TreeViewItem();
            newChild.Header = "Цель: " + goal.Var.Name;
            explanationTree.Items.Add(newChild);

            foreach (var rule in workedRules)
            {
                TreeViewItem child = new TreeViewItem();
                child.Header = rule.Name + ": " + rule.Content;
                child.IsSelected = true;
                newChild.Items.Add(child);
                TreeViewItem ch = new TreeViewItem();
                ch.Header = rule.Explanation.ToString();
                ch.IsSelected = true;
                child.Items.Add(ch);
            }
            newChild = new TreeViewItem();
            newChild.Header = "Результат: " + goal.Var.Name + " = " + goal.Value;
            newChild.IsSelected = true;
            explanationTree.Items.Add(newChild);

            lbMemory.Items.Clear();
            foreach (var v in memory)
                lbMemory.Items.Add("Переменная: " + v.Var + " = " + v.Value);

            SetTreeViewItems(explanationTree, false);
        }
        void SetTreeViewItems(object obj, bool expand)
        {
            if (obj is TreeViewItem)
            {
                ((TreeViewItem)obj).IsExpanded = expand;
                foreach (object obj2 in ((TreeViewItem)obj).Items)
                    SetTreeViewItems(obj2, expand);
            }
            else if (obj is ItemsControl)
            {
                foreach (object obj2 in ((ItemsControl)obj).Items)
                {
                    if (obj2 != null)
                    {
                        SetTreeViewItems(((ItemsControl)obj).ItemContainerGenerator.ContainerFromItem(obj2), expand);

                        TreeViewItem item = obj2 as TreeViewItem;
                        if (item != null)
                            item.IsExpanded = expand;
                    }
                }
            }
        }

        private void expCollapseLink_Click_1(object sender, RoutedEventArgs e)
        {
            expCollapseLink.Content = isExpanded ?
               "(скрыть все)" : "(раскрыть все)";

            SetTreeViewItems(explanationTree, isExpanded);
            isExpanded = !isExpanded;
        }
    }
}
