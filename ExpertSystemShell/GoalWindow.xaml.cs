﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell
{
    /// <summary>
    /// Логика взаимодействия для GoalWindow.xaml
    /// </summary>
    public partial class GoalWindow : Window
    {
        private Variable var;
        
        public Variable Goal
        {
            get { return var; }
            set { var = value; }
        }

        public GoalWindow()
        {
            InitializeComponent();
            cbInit();
        }

        private void cbInit()
        {
            cbGoal.Items.Clear();
            foreach (Variable d in MainWindow.CurrentShell.CurKnowledgeBase.variables)
                if (d.Type == VariableEnum.Out || d.Type == VariableEnum.InOut)
                    cbGoal.Items.Add(d.Name);
            cbGoal.SelectedIndex = 0;
        }

        public GoalWindow(Variable var)
        {
            InitializeComponent();
            cbInit();
            this.var = var;
            //cbGoal.SelectedItem = var;
              cbGoal.SelectedIndex = MainWindow.CurrentShell.CurKnowledgeBase.variables
                 .IndexOf(MainWindow.CurrentShell.CurKnowledgeBase.variables
                 .Where(element => element == var).FirstOrDefault());

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (cbGoal.SelectedIndex >= 0)
            {
                // string g = cbGoal.SelectedItem as string;

                // Goal = MainWindow.CurrentShell.CurKnowledgeBase.variables
                //.Where(element => element.Name == g).FirstOrDefault();


                string value = cbGoal.SelectedItem as string;
                if (value != null)
                {
                    var = MainWindow.CurrentShell.CurKnowledgeBase.variables.Where(element => value == element.Name).First();
                    this.DialogResult = true;
                    Close();
                }
                else
                {

                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
