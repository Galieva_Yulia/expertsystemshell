﻿using ExpertSystemShell.Ke;
using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using ExpertSystemShell.DragAndDrop;

namespace ExpertSystemShell
{
    public enum WindowMode
    {
        add,
        edit
    }
    public class MyItem
    {
        public int Number { get; set; }         //порядковый номер значения
        public string Name { get; set; }
        public string Type { get; set; }
        public string Content { get; set; }
    }
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Shell CurrentShell { get; set; }
        private Variable goal;
        ListViewDragDropManager<Domain> dragMgrDom;
        ListViewDragDropManager<Variable> dragMgrVar;
        ListViewDragDropManager<Rule> dragMgrRule;
        public MainWindow()
        {
            CurrentShell = new Shell();
            InitializeComponent();
            lvDomains.ItemsSource = CurrentShell.CurKnowledgeBase.domains;
            lvVariables.ItemsSource = CurrentShell.CurKnowledgeBase.variables;
            lvRules.ItemsSource = CurrentShell.CurKnowledgeBase.rules;


            this.dragMgrDom = new ListViewDragDropManager<Domain>(this.lvDomains);
            this.dragMgrVar = new ListViewDragDropManager<Variable>(this.lvVariables);
            this.dragMgrRule = new ListViewDragDropManager<Rule>(this.lvRules);
            this.lvDomains.DragEnter += OnListViewDragEnter;
            this.lvVariables.DragEnter += OnListViewDragEnter;
            this.lvRules.DragEnter += OnListViewDragEnter;

            this.Closing += MainWindow_Closing;
        }

        private void OnListViewDrop(object sender, DragEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void OnListViewDragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Move;
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Сохранить изменения перед выходом?", "Внимание",
            MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
                SaveExpertSystem();

        }
        #region Domain 
        private void btnAddDomain_Click(object sender, RoutedEventArgs e)
        {
            openDomainEditingWindow(WindowMode.add);
        }

        private void btnEditDomain_Click(object sender, RoutedEventArgs e)
        {
            openDomainEditingWindow(WindowMode.edit);
        }

        private void btnDeleteDomain_Click(object sender, RoutedEventArgs e)
        {
            int index = lvDomains.SelectedIndex;
            if (index == -1)
                return;

            Domain dom = CurrentShell.CurKnowledgeBase.domains[index];
            bool isUsed = false;
            isUsed = DomainEditingWindow.checkDomenforUsing(dom);

            if (isUsed)
                MessageBox.Show("Домен используется в правиле, удаление невозможно");
            else
            {
                if (index != -1)
                    CurrentShell.CurKnowledgeBase.DeleteDomain(index);
            }
        }

        private void openDomainEditingWindow(WindowMode mode)
        {

            DomainEditingWindow domainEditingWindow = null;

            if (mode == WindowMode.add)
                domainEditingWindow = new DomainEditingWindow();
            else
            {
                int index = lvDomains.SelectedIndex;
                if (index == -1)
                    return;

                Domain domain = CurrentShell.CurKnowledgeBase.domains[index];

                bool isUsed = DomainEditingWindow.checkDomenforUsing(domain);

                if (isUsed)
                {
                    MessageBoxResult result = MessageBox.Show("Выбранный домен используется в правиле, поэтому доступна только опция добавления новых значений в домен.Создать новый домен на основе выбранного?", "Внимание",
                    MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (result == MessageBoxResult.Yes)
                    {
                        mode = WindowMode.add;
                        Domain d2 = new Domain();
                        d2.Name = domain.Name + "copy";
                        d2.Values = domain.Values.ToList();
                        domainEditingWindow = new DomainEditingWindow(d2);
                    }
                    else
                    {
                        if (domain != null)
                            domainEditingWindow = new DomainEditingWindow(domain);
                        else
                            return;
                    }
                }
            }
            domainEditingWindow.Owner = this;

            if (domainEditingWindow.ShowDialog() == true)
            {
                CurrentShell.CurKnowledgeBase.AddOrEditDomain(domainEditingWindow.domainCopy, mode, lvDomains.SelectedIndex);
            }
        }
        
        private void ListViewItem_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Domain d = lvDomains.SelectedItem as Domain;
            if (d != null)
                ListViewOp.FillListView_NumberAndContent(d.Values, lvCurrentDomain);
        }

        private void LVRules_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Rule result = lvRules.SelectedItem as Rule;
            if (result != null)
            {
                ListViewOp.UpdateLv(result.ConditionList, lvRuleCondition, "==");
                ListViewOp.UpdateLv(result.ConclusionList, lvRuleConclusion, ":=");
            }
        }

        #endregion
        #region Variable
        private void LVVariables_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Variable var = lvVariables.SelectedItem as Variable;
            if (var != null)
            {
                tbVarDomain.Text = var.Dom.Name;
                if (var.Type == VariableEnum.In)
                    tbVarType.Text = "Запрашиваемая";
                if (var.Type == VariableEnum.Out)
                    tbVarType.Text = "Выводимая";
                if (var.Type == VariableEnum.InOut)
                    tbVarType.Text = "Запрашиваемо - выводимая";
                tbQuestion.Text = var.Text;
            }
            
        }

        private void btnAddVar_Click(object sender, RoutedEventArgs e)
        {
            openVarEditingWindow(WindowMode.add);
        }

        private void btnEditVar_Click(object sender, RoutedEventArgs e)
        {
            int index = lvVariables.SelectedIndex;
            if (index == -1)
                return;
            Variable var = CurrentShell.CurKnowledgeBase.variables[index];
            bool isUsed = VarEditingWindow.CheckForUsing(var);
            openVarEditingWindow(WindowMode.edit);
        }

        public void openVarEditingWindow(WindowMode mode)
        {
            VarEditingWindow varEditingWindow;

            if (mode == WindowMode.add)
                varEditingWindow = new VarEditingWindow();
            else
            {
                Variable var = lvVariables.SelectedItem as Variable;
                if (var != null)
                    varEditingWindow = new VarEditingWindow(var);
                else
                {
                    MessageBox.Show("Выберете переменную для редактирования");
                    return;
                }
            }
            varEditingWindow.Owner = this;

            if (varEditingWindow.ShowDialog() == true)
            {
                CurrentShell.CurKnowledgeBase.AddOrEditVariable(varEditingWindow.varCopy, mode, lvVariables.SelectedIndex);
            }
        }


        private void btnDeleteVar_Click(object sender, RoutedEventArgs e)
        {
            int index = lvVariables.SelectedIndex;
            if (index == -1)
                return;

            Variable var = CurrentShell.CurKnowledgeBase.variables[index];

            bool isUsed = VarEditingWindow.CheckForUsing(var);
            if (isUsed)
                MessageBox.Show("Переменная используется в правиле, удаление невозможно");
            else
            {
                if (index != -1)
                    CurrentShell.CurKnowledgeBase.DeleteVars(index);
            }
        }
        #endregion
        #region Rule
        private void btnAddRule_Click(object sender, RoutedEventArgs e)
        {
            openRuleEditingWindow(WindowMode.add);
        }

        private void openRuleEditingWindow(WindowMode mode)
        {
            RulesEditingWindow ruleEditingWindow;

            if (mode == WindowMode.add)
                ruleEditingWindow = new RulesEditingWindow();
            else
            {
                //if (lvRules.SelectedItems.Count <= 0)
                //{
                //    MessageBox.Show("Выберете переменную для редактирования");
                //    return;
                //}

                Rule rule = lvRules.SelectedItem as Rule;
                if (rule != null)
                    ruleEditingWindow = new RulesEditingWindow(rule);                
                else
                {
                    MessageBox.Show("Выберете правило для редактирования");
                    return;
                }
            }
            ruleEditingWindow.Owner = this;

            if (ruleEditingWindow.ShowDialog() == true)
            {
                CurrentShell.CurKnowledgeBase.AddOrEditRule(ruleEditingWindow.ruleCopy, mode, lvRules.SelectedIndex);
                UpdateRulesLv();
            }
        }

        private void UpdateRulesLv()
        {
            lvRuleCondition.Items.Clear();
            lvRuleConclusion.Items.Clear();
        }
        #endregion

        private void btnEditRule_Click(object sender, RoutedEventArgs e)
        {
            openRuleEditingWindow(WindowMode.edit);
        }

        private void miSave_Click(object sender, RoutedEventArgs e)
        {
            SaveExpertSystem();
        }

        private void SaveExpertSystem()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "ExpertSys" + DateTime.Now.ToString(); // Default file name
            dlg.DefaultExt = ".dat"; // Default file extension
            dlg.Filter = "Text documents (.dat)|*.dat"; // Filter files by extension

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                string fileName = dlg.FileName;
                try
                {
                    CurrentShell.SaveSystem(fileName);
                }
                catch (ArgumentException ex)
                {
                   // int num = (int)MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
        }

        private void miOpen_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "ExpertSys"; // Default file name

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                try
                {
                    CurrentShell.LoadSystem(filename);
                    lvDomains.ItemsSource = CurrentShell.CurKnowledgeBase.domains;
                    lvVariables.ItemsSource = CurrentShell.CurKnowledgeBase.variables;
                    lvRules.ItemsSource = CurrentShell.CurKnowledgeBase.rules;
                }
                catch (ArgumentException ex)
                {
                   // int num = (int)MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
        }

        private void miSetGoal_Click(object sender, RoutedEventArgs e)
        {
            GoalWindow goalWindow;
            if (goal == null)
                goalWindow = new GoalWindow();
            else
                goalWindow = new GoalWindow(goal);

            goalWindow.Owner = this;

            if (goalWindow.ShowDialog() == true)
            {
                goal = goalWindow.Goal;
            }
        }

        private void miStartConsult_Click(object sender, RoutedEventArgs e)
        {
            if (goal == null)
                MessageBox.Show("Необходимо задать цель консультации");
            else
                StartConsult(goal);
        }

        private void StartConsult(Variable goal)
        {
            ConsultWindow consWindow = new ConsultWindow(goal);
            if (consWindow.ShowDialog() == true)
            {
                CurrentShell.curConsultation = consWindow.consultation;
            }
        }

        private void miShowExplanation_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentShell.curConsultation == null)
                MessageBox.Show("Необходимо пройти консультацию");
            else
            {
                ExplanationTreeWindow treeWindow = new ExplanationTreeWindow(CurrentShell.curConsultation.goal, CurrentShell.curConsultation.workedRules, CurrentShell.curConsultation.memory);
                if (treeWindow.ShowDialog() == true)
                {
                    //CurrentShell.curConsultation = consWindow.consultation;
                }
            }
        }

        private void miNew_Click(object sender, RoutedEventArgs e)
        {
            CurrentShell.CurKnowledgeBase = new KnowledgeBase();
            lvDomains.ItemsSource = CurrentShell.CurKnowledgeBase.domains;
            lvVariables.ItemsSource = CurrentShell.CurKnowledgeBase.variables;
            lvRules.ItemsSource = CurrentShell.CurKnowledgeBase.rules;
        }

        private void btnDeleteRule_Click(object sender, RoutedEventArgs e)
        {
            int index = lvRules.SelectedIndex;
            if (index == -1)
                return;

            Rule var = CurrentShell.CurKnowledgeBase.rules[index];

            if (index != -1)
                CurrentShell.CurKnowledgeBase.DeleteRules(index);
        }
    }
}
