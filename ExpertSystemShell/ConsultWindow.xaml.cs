﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell
{
    /// <summary>
    /// Логика взаимодействия для ConsultWindow.xaml
    /// </summary>
    public partial class ConsultWindow : Window
    {
        public Consultation consultation { get; set; }
        public string Answer = "";
        Thread ConsultThread;
        bool isError = false;
        public ConsultWindow()
        {
            InitializeComponent();

        }
        public ConsultWindow(Variable var)
        {
            InitializeComponent();
            consultation = new Consultation(var, MainWindow.CurrentShell.CurKnowledgeBase);
            lbGoal.Content = consultation.goal.Var.Name.ToString();

            ConsultThread = new Thread(delegate ()
            {
                StartConsultation();
            });
            ConsultThread.Start();
            this.Closing += ConsultWindow_Closing;

        }

        private void ConsultWindow_Closing(object sender, CancelEventArgs e)
        {
            this.DialogResult = true;
            try
            {
                ConsultThread.Abort();
            }
            catch
            {
                ConsultThread.Resume();
                ConsultThread.Abort();
            }
        }

        private void StartConsultation()
        {
            getNext(consultation.goal, 0); //начинаем просмотр с первого правила
                                           // if (!IsNotError) MainGoal.Value = "Система не смогла найти ответ, обратитесь к другой системе!";

            
            if (!isError)
            {
                consultation.goal.Value = consultation.memory.Find(x => x.Var == consultation.goal.Var).Value;

                Dispatcher.Invoke(delegate
                {
                    ShowResult(consultation.goal, consultation.workedRules);
                });
               
            }
            if (isError)
                MessageBox.Show("Данная экспертная система не нашла ответ, обратитесь к другой системе или проверьте правильность составления правил");

        }

        private void ShowResult(Fact goal, List<Rule> workedRules)
        {
            lbConsultation.Items.Add("Результат консультации:");
            if (isError)
                lbConsultation.Items.Add("Данная экспертная система не нашла ответ, обратитесь к другой системе");
            else
            lbConsultation.Items.Add(goal.Var + " = " + goal.Value);
        }

      
        private void getNext(Fact curGoal, int index)
        {
            int count = MainWindow.CurrentShell.CurKnowledgeBase.rules.Count;
            //Ищем правило, которое означит текущую переменную
            for (; index < count; index++)
            {
                Fact fact = MainWindow.CurrentShell.CurKnowledgeBase.rules[index].ConclusionList.Find(x => x.Var == curGoal.Var);
                if (fact != null && fact.Var == curGoal.Var)
                    break; //если нашли - выходим из цикла
            }
            //Если правило не найдено - выход
            if (index == count && curGoal.Var.Type != VariableEnum.InOut)
            {
                isError = true;
                return;
            }
            else
            {
                if (curGoal.Var.Type == VariableEnum.InOut)
                {
                    Dispatcher.Invoke(delegate
                    {
                        WaitForAnswer(curGoal);
                    });
                    curGoal.Value = Answer;
                    consultation.memory.Add(curGoal);
                    return;
                }
            }

            //мы нашли правило, где нужная переменая означивается
            //теперь надо проверить ее посылки
            bool ruleIsWorked = false;
            foreach (var curFact in MainWindow.CurrentShell.CurKnowledgeBase.rules[index].ConditionList)
            {
                Fact curFactCopy = consultation.memory.Find(x => x.Var == curFact.Var);
                //если нет в РП
                if (!consultation.memory.Exists(element => element.Var == curFact.Var))
                {
                    curFactCopy = new Fact(); //я не могу curFact изменять, так как он в фориче
                    curFactCopy.Var = curFact.Var;

                    //если нет в РП, то 1) если запр, то спрашиваем 
                    if (curFact.Var.Type == VariableEnum.In)
                    {
                        Dispatcher.Invoke(delegate
                        {
                            WaitForAnswer(curFact);
                        });
                        curFactCopy.Value = Answer;
                        consultation.memory.Add(curFactCopy); //добавила означившуюся переменную со значение  РП
                    }
                    else
                    {
                        //2)смотрим в заключениях правил
                        getNext(curFactCopy, 0);
                        curFactCopy = consultation.memory.Find(x => x.Var == curFact.Var);
                    }
                }

                if (curFactCopy == null)
                {
                    isError = true;
                    return;
                }
                //если не равно, нужно искать новое правило (после текущего)
                if (curFactCopy.Value != curFact.Value)
                {
                    ruleIsWorked = false;
                    getNext(curGoal, index + 1);
                    break;
                }
                else
                    ruleIsWorked = true;
            }

            //Если правило сработало - означиваем заключения
            
            if (ruleIsWorked)
            {
                consultation.workedRules.Add(MainWindow.CurrentShell.CurKnowledgeBase.rules[index]);
                foreach (var res in MainWindow.CurrentShell.CurKnowledgeBase.rules[index].ConclusionList)
                {
                    consultation.memory.Add(res);
                }
            }

            if (isError) return;
            //Если текущая означена, сохраним - выйдем из рек. к след.
            if (curGoal.Value != "")
                return;
        }

        private void WaitForAnswer(Fact curGoal)
        {
            ConsultThread.Suspend(); //приостанавливаем поток

            if (String.IsNullOrWhiteSpace(curGoal.Var.Text))
                lbConsultation.Items.Add("Вопрос: " + curGoal.Var.Name + ":");
            else
                lbConsultation.Items.Add("Вопрос: " + curGoal.Var.Text);

            cbUpdate(curGoal);
        }

        private void cbUpdate(Fact curGoal)
        {
            cbAnswer.Items.Clear();
            foreach (string v in curGoal.Var.Dom.Values)
                cbAnswer.Items.Add(v);

            cbAnswer.SelectedIndex = 0;
        }

        private void btnAnswer_Click(object sender, RoutedEventArgs e)
        {
            if (consultation.goal.Value != null)
                return;
            Answer = cbAnswer.SelectedItem as string;
            if (Answer != null)
                lbConsultation.Items.Add("Ответ: " + Answer);
            ConsultThread.Resume();

        }

        private void btnFinishConsult_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            Close();
        }

        private void btnShowConsult_Click(object sender, RoutedEventArgs e)
        {
            ExplanationTreeWindow treeWindow = new ExplanationTreeWindow(consultation.goal, consultation.workedRules,consultation.memory);
            if (treeWindow.ShowDialog() == true)
            {
                //CurrentShell.curConsultation = consWindow.consultation;
            }
        }
    }
}
