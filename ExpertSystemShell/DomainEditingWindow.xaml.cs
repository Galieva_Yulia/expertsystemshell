﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace ExpertSystemShell
{
    /// <summary>
    /// Логика взаимодействия для DomainEditingWindow.xaml
    /// </summary>
    public partial class DomainEditingWindow : Window
    {
        public Domain domain { get; set; }
        public Domain domainCopy { get;}
        public DomainEditingWindow()
        {
            InitializeComponent();
            domain = new Domain();
            domainCopy = new Domain();
            tbDomainName.Text = "Domen" + (MainWindow.CurrentShell.CurKnowledgeBase.domains.Count() + 1).ToString();
            tbDomainName.Focus();
        }

        public DomainEditingWindow(Domain d)
        {
            InitializeComponent();
            domain = d;

            domainCopy = new Domain();
            for (int i = 0; i< d.Values.Count; i++)
            {
                string str = d.Values[i];
                domainCopy.Values.Add(str);
            }
            domainCopy.Name = d.Name.ToString();
            Initialize(d);
        }

        private void Initialize(Domain d)
        {
            if (checkDomenforUsing(d))
            {
                tbDomainName.IsEnabled = false;
                tbEditDomainValue.IsEnabled = false;
                tbDeleteDomainValue.IsEnabled = false;
            }
            tbDomainName.Text = domain.Name;
            tbDomainName.Focus();
            ListViewOp.FillListView_NumberAndContent(domain.Values, lvDomenValues);
            lvDomenValues.SelectedIndex = 0;
        }

        private void tbAddDomainValue_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbValue.Text))
            {
                MessageBox.Show("Значение не должно быть пустым");
                return;
            }

            if (!domainCopy.Values.Exists(element => element == tbValue.Text))
            {
                domainCopy.Values.Add(tbValue.Text);
                tbValue.Text = "";
            }
            ListViewOp.FillListView_NumberAndContent(domainCopy.Values, lvDomenValues);
        }

        private void ListViewItem_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null && item.IsSelected)
            {
                MyItem it = (MyItem)item.Content;
                tbValue.Text = it.Content;
            }
        }

        private void tbEditDomainValue_Click(object sender, RoutedEventArgs e)
        {
            if (lvDomenValues.SelectedIndex != -1)
            {
                domainCopy.Values[lvDomenValues.SelectedIndex] = tbValue.Text;
            }
            ListViewOp.FillListView_NumberAndContent(domainCopy.Values, lvDomenValues);
        }

        private void tbDeleteDomainValue_Click(object sender, RoutedEventArgs e)
        {
            if (lvDomenValues.SelectedIndex != -1)
            {
                domainCopy.Values.RemoveAt(lvDomenValues.SelectedIndex);
            }
            ListViewOp.FillListView_NumberAndContent(domainCopy.Values, lvDomenValues);
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbDomainName.Text))
            {
                MessageBox.Show("Значение не должно быть пустым");
                return;
            }
            else
            {
                domainCopy.Name = tbDomainName.Text;
                this.DialogResult = true;
                Close();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public static bool checkDomenforUsing(Domain dom)
        {
            bool isUsed = false;
            for (int i = 0; i < MainWindow.CurrentShell.CurKnowledgeBase.rules.Count; i++)
            {

                foreach (Fact f in MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConditionList)
                {
                    if (f.Var.Dom == dom)
                        isUsed = true;
                }

                foreach (Fact f in MainWindow.CurrentShell.CurKnowledgeBase.rules[i].ConclusionList)
                {
                    if (f.Var.Dom == dom)
                        isUsed = true;
                }
            }

            return isUsed;
        }
    }
}
